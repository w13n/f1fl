use std::{error::Error, path::PathBuf};

use clap::Args;
use rusqlite::Connection;

#[derive(Args)]
pub struct Arguments {
    /// Path to this season's database
    database_path: PathBuf,
    /// Which round's drivers to swap
    round: usize,
}

struct Config {
    conn: Connection,
    round: usize,
    driver_count: usize,
}

impl Config {
    pub fn build(args: Arguments) -> Result<Config, Box<dyn Error>> {
        let conn = Connection::open(args.database_path)?;
        let round = args.round;

        let mut dc_row =
            conn.prepare("SELECT \"DATA\" FROM \"META\" WHERE \"KEY\" IS \"DRIVER COUNT\"")?;
        let driver_count = dc_row
            .query_row([], |row| row.get::<usize, String>(0))?
            .parse::<usize>()?;
        drop(dc_row);

        Ok(Config {
            conn,
            round,
            driver_count,
        })
    }
}

pub fn run(args: Arguments) -> Result<(), Box<dyn Error>> {
    let config = Config::build(args)?;
    let teams = super::get_teams(&config.conn)?;
    let round = config.round;

    let team_index = super::user_in_menu("Which team to swap a driver?", &teams);
    let team = teams.get(team_index).unwrap();
    let drivers = super::get_drivers(&config.conn, team, config.round, config.driver_count)?;

    let mut driver_index = super::user_in_menu("Which driver to swap?", &drivers);
    let driver = drivers.get(driver_index).unwrap();
    driver_index += 1;

    println!("What driver to replace {driver} with?");
    let new_driver = super::user_in::<usize>();

    config.conn.execute(&format!("UPDATE \"TEAM {team}\" SET \"driver{driver_index}\"={new_driver} WHERE \"RACEID\"={round};"), ())?;

    Ok(())
}
