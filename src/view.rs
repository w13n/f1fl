use std::{cmp::Ordering, error::Error, path::PathBuf};

use clap::Args;
use rusqlite::Connection;

#[derive(Args)]
pub struct Arguments {
    /// Path to this season's database
    database_path: PathBuf,
    /// Which round to view
    round: usize,
}

struct Config {
    conn: Connection,
    round: usize,
    driver_count: usize,
}

impl Config {
    pub fn build(args: Arguments) -> Result<Config, Box<dyn Error>> {
        let conn = Connection::open(args.database_path)?;
        let round = args.round;

        let mut dc_row =
            conn.prepare("SELECT \"DATA\" FROM \"META\" WHERE \"KEY\" IS \"DRIVER COUNT\"")?;
        let driver_count = dc_row
            .query_row([], |row| row.get::<usize, String>(0))?
            .parse::<usize>()?;
        drop(dc_row);

        Ok(Config {
            conn,
            round,
            driver_count,
        })
    }
}

#[derive(Debug, Eq, PartialEq)]
struct Team {
    name: String,
    score: isize,
    tiebreaker: isize,
    drivers: Vec<u32>,
}

impl Ord for Team {
    fn cmp(&self, other: &Self) -> Ordering {
        if self.score == other.score {
            return self.tiebreaker.cmp(&other.tiebreaker);
        }
        self.score.cmp(&other.score)
    }
}

impl PartialOrd for Team {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

pub fn run(args: Arguments) -> Result<(), Box<dyn Error>> {
    let config = Config::build(args)?;
    let round = config.round;

    let team_names = super::get_teams(&config.conn)?;
    let mut teams = Vec::with_capacity(team_names.len());

    let mut max_length = 0;

    for name in team_names {
        if name.len() > max_length {
            max_length = name.len();
        }

        let score = config.conn.query_row(
            &format!("SELECT \"{name}\" FROM \"SCORES\" WHERE RACEID=0"),
            (),
            |row| row.get::<usize, isize>(0),
        )?;

        let tiebreaker = config.conn.query_row(
            &format!("SELECT Max(\"{name}\") FROM \"SCORES\" WHERE RACEID IS NOT 0"),
            (),
            |row| row.get::<usize, isize>(0),
        )?;

        let drivers = super::get_drivers(&config.conn, &name, round, config.driver_count)?;

        teams.push(Team {
            name,
            score,
            tiebreaker,
            drivers,
        });
    }

    teams.sort();
    teams.reverse();

    for (place, team) in teams.iter().enumerate() {
        let whitespace = " ".repeat(max_length - team.name.len());
        println!(
            "{}: {} {}| {}",
            place + 1,
            team.name,
            whitespace,
            team.score,
        );
    }
    println! {};

    for team in teams {
        let whitespace = " ".repeat(max_length - team.name.len());
        print!("{}{}:", team.name, whitespace);
        for driver in team.drivers {
            if driver < 10 {
                print!(" ")
            }
            print!(" {driver}");
        }
        println! {};
    }
    Ok(())
}
