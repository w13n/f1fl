use std::{error::Error, path::PathBuf};

use clap::Args;
use rusqlite::Connection;

#[derive(Args)]
pub struct Arguments {
    /// Path to this season's database
    database_path: PathBuf,
    /// Which round to draft
    round: usize,
}

struct Config {
    conn: Connection,
    round: usize,
    driver_count: usize,
}

impl Config {
    pub fn build(args: Arguments) -> Result<Config, Box<dyn Error>> {
        let conn = Connection::open(args.database_path)?;
        let round = args.round;

        let mut dc_row =
            conn.prepare("SELECT \"DATA\" FROM \"META\" WHERE \"KEY\" IS \"DRIVER COUNT\"")?;
        let driver_count = dc_row
            .query_row([], |row| row.get::<usize, String>(0))?
            .parse::<usize>()?;
        drop(dc_row);

        Ok(Config {
            conn,
            round,
            driver_count,
        })
    }
}

pub fn run(args: Arguments) -> Result<(), Box<dyn Error>> {
    let config = Config::build(args)?;
    let round = config.round;

    let teams = super::get_teams(&config.conn)?;
    let past = round - 1;

    for team in teams {
        let mut drivers = super::get_drivers(&config.conn, &team, past, config.driver_count)?;

        drivers.pop();

        println!("who did {team} draft?");
        let number = super::user_in::<u32>();
        drivers.insert(0, number);

        let mut command = format! {"INSERT INTO \"TEAM {team}\" VALUES({round},"};
        for num in drivers {
            command.push_str(&format!("{num},"));
        }
        command.pop();
        command.push_str(");");

        config.conn.execute(&command, ())?;
    }

    Ok(())
}
