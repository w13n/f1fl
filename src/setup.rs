use std::{error::Error, path::PathBuf};

use clap::Args;
use rusqlite::Connection;

#[derive(Args)]
pub struct Arguments {
    /// Path to an existing SQLite DB, or where one should be created
    database_path: PathBuf,
    /// The year that this season will take place in
    year: usize,
    /// Number of teams
    team_count: usize,
    /// Number of drivers per team
    driver_count: usize,
}

struct Config {
    conn: Connection,
    year: usize,
    team_count: usize,
    driver_count: usize,
}

impl Config {
    pub fn build(args: Arguments) -> Result<Config, Box<dyn Error>> {
        let conn = Connection::open(args.database_path)?;
        let year = args.year;
        let team_count = args.team_count;
        let driver_count = args.driver_count;

        Ok(Config {
            conn,
            year,
            team_count,
            driver_count,
        })
    }
}

pub fn run(args: Arguments) -> Result<(), Box<dyn Error>> {
    let config = Config::build(args)?;

    create_meta_table(&config)?;
    create_score_table(&config)?;
    create_team_tables(&config)?;
    setup_tables(&config)?;

    Ok(())
}

fn create_meta_table(config: &Config) -> Result<(), Box<dyn Error>> {
    let year = config.year;
    let team_count = config.team_count;
    let driver_count = config.driver_count;
    let version = env!("CARGO_PKG_VERSION");

    config.conn.execute(
        "CREATE TABLE \"META\" (
                            KEY TEXT PRIMARY KEY,
                            DATA TEXT);",
        (),
    )?;

    config.conn.execute(
        &format! {"INSERT INTO \"META\" VALUES
                    (\"YEAR\", \"{year}\"),
                    (\"TEAM COUNT\", \"{team_count}\"),
                    (\"DRIVER COUNT\", \"{driver_count}\"),
                    (\"VERSION\", \"{version}\");"},
        (),
    )?;

    Ok(())
}

fn create_score_table(config: &Config) -> Result<(), Box<dyn Error>> {
    let conn = &config.conn;
    let team_count = config.team_count;

    let mut command = String::from("CREATE TABLE \"SCORES\" (RACEID INT PRIMARY KEY,");

    for i in 1..=team_count {
        println!("what is the name for team {i} of {team_count}");
        let name = super::user_in::<String>();
        command.push_str(&format!("\"{name}\" INT,"));
    }

    command.pop();
    command.push_str(");");
    conn.execute(&command, ())?;

    Ok(())
}

fn create_team_tables(config: &Config) -> Result<(), Box<dyn Error>> {
    let conn = &config.conn;

    let teams = super::get_teams(conn)?;
    for i in teams {
        let mut command = format! {"CREATE TABLE \"TEAM {i}\" (RACEID INT PRIMARY KEY,"};

        for j in 1..=config.driver_count {
            command.push_str(&format!("driver{j} INT,"))
        }
        command.pop();
        command.push_str(");");

        conn.execute(&command, ())?;
    }
    Ok(())
}

fn setup_tables(config: &Config) -> Result<(), Box<dyn Error>> {
    let conn = &config.conn;

    let mut command = String::from("INSERT INTO \"SCORES\" VALUES (");
    command.push_str(&"0,".repeat(config.team_count + 1));
    command.pop();
    command.push_str(");");
    conn.execute(&command, ())?;

    let teams = super::get_teams(conn)?;
    for i in teams {
        let mut command = format!("INSERT INTO \"TEAM {i}\" VALUES (1,");

        for j in 1..=config.driver_count {
            println!("who is driver #{j} of team: {i}");
            let number = super::user_in::<usize>();
            command.push_str(&format!("{number},"));
        }

        command.pop();
        command.push_str(");");
        conn.execute(&command, ())?;
    }

    Ok(())
}
