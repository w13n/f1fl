use std::{error::Error, io, str::FromStr, fmt::Display};

use clap::{Parser, Subcommand};
use rusqlite::Connection;

mod draft;
mod score;
mod setup;
mod skip;
mod swap;
mod view;

pub fn run(cli: Cli) -> Result<(), Box<dyn Error>> {
    match cli.command {
        Command::Setup(args) => setup::run(args),
        Command::View(args) => view::run(args),
        Command::Draft(args) => draft::run(args),
        Command::Skip(args) => skip::run(args),
        Command::Score(args) => score::run(args),
        Command::Swap(args) => swap::run(args),
    }
}

#[derive(Parser)]
#[command(author, version, about, long_about = None)]
pub struct Cli {
    #[command(subcommand)]
    command: Command,
}

#[derive(Subcommand)]
enum Command {
    /// Setup a database for a season
    Setup(setup::Arguments),
    /// View the drivers and points for every team
    View(view::Arguments),
    /// Draft new drivers for every team
    Draft(draft::Arguments),
    /// Skip drafting for a round
    Skip(skip::Arguments),
    /// Score a round
    Score(score::Arguments),
    /// Swap a driver on a team for a new driver
    Swap(swap::Arguments),
}

// global helpers

pub fn user_in<T: FromStr>() -> T {
    loop {
        let mut value = String::new();
        io::stdin().read_line(&mut value).unwrap();
        if !value.trim().is_empty() {
            if let Ok(value) = value.trim().parse::<T>() {
                return value;
            }
        }
        eprintln!("could not parse value. please try again")
    }
}

pub fn user_in_menu<D: Display>(prompt: &str, choices: &Vec<D>)  -> usize 
{
    println!("{prompt}");
    for (i, choice) in choices.iter().enumerate() {
        println!("{i}: {choice}");
    }
    loop {
        let pick = user_in::<usize>();
        if pick < choices.len() {
            break pick
        }
        eprintln!("value greater than menu options. please try again")
    }
}

pub fn get_teams(conn: &Connection) -> Result<Vec<String>, rusqlite::Error> {
    let headers = conn.prepare("SELECT * FROM \"SCORES\"")?;
    let mut teams = headers.column_names();
    teams.remove(0);
    let teams = teams.into_iter().map(String::from).collect();
    Ok(teams)
}

pub fn get_drivers(
    conn: &Connection,
    team: &String,
    raceid: usize,
    driver_count: usize,
) -> Result<Vec<u32>, rusqlite::Error> {
    conn.query_row(
        &format!("SELECT * FROM \"TEAM {team}\" WHERE RACEID={raceid}"),
        (),
        |row| {
            let mut drivers = Vec::with_capacity(driver_count);
            for i in 1..=driver_count {
                drivers.push(row.get::<usize, u32>(i)?);
            }
            Ok(drivers)
        },
    )
}
