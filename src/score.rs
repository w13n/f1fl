use std::{error::Error, path::PathBuf};

use clap::Args;
use ergast_rs::{
    self,
    apis::race_table::{RaceResult, RaceTable},
    Ergast, ErgastClient,
};
use rusqlite::Connection;

#[derive(Args)]
pub struct Arguments {
    /// Path to this season's database
    database_path: PathBuf,
    /// Which round to score
    round: u32,
    /// Deletes any conflicting score before calculating
    #[arg(short, long)]
    force: bool
}
struct Config {
    conn: Connection,
    round: u32,
    year: u32,
    driver_count: usize,
    force: bool
}

impl Config {
    pub fn build(args: Arguments) -> Result<Config, Box<dyn Error>> {
        let conn = Connection::open(args.database_path)?;
        let round = args.round;

        let mut year_row =
            conn.prepare("SELECT \"DATA\" FROM \"META\" WHERE \"KEY\" IS \"YEAR\"")?;
        let year = year_row
            .query_row([], |row| row.get::<usize, String>(0))?
            .parse::<u32>()?;
        drop(year_row);

        let mut dc_row =
            conn.prepare("SELECT \"DATA\" FROM \"META\" WHERE \"KEY\" IS \"DRIVER COUNT\"")?;
        let driver_count = dc_row
            .query_row([], |row| row.get::<usize, String>(0))?
            .parse::<usize>()?;
        drop(dc_row);

        let force = args.force;

        Ok(Config {
            conn,
            round,
            year,
            driver_count,
            force,
        })
    }
}

pub fn run(args: Arguments) -> Result<(), Box<dyn Error>> {
    let config = Config::build(args)?;
    let round = config.round;

    let teams = super::get_teams(&config.conn)?;
    let client = ergast_rs::ErgastClient::new()?;
    let rt = get_race_table(&client, config.round, config.year)?;
    let results = get_race_results(&rt)?;

    if config.force {
        config.conn.execute(
            &format! {"DELETE FROM \"SCORES\" WHERE RACEID={round}"},
            (),
        )?;
    }

    config.conn.execute(
        &format! {"INSERT INTO \"SCORES\" (\"RACEID\") VALUES ({round})"},
        (),
    )?;

    for team in teams {
        let mut score = 0;
        let drivers = super::get_drivers(
            &config.conn,
            &team,
            config.round as usize,
            config.driver_count,
        )?;

        for driver in drivers {
            score += score_og(results, driver)?
        }

        config.conn.execute(
            &format!("UPDATE \"SCORES\" SET \"{team}\"={score} WHERE \"RACEID\"={round};"),
            (),
        )?;
        let mut total_stmt = config.conn.prepare(&format!(
            "SELECT \"{team}\" FROM \"SCORES\" WHERE \"RACEID\" IS NOT 0"
        ))?;
        let total_vec = total_stmt.query_map([], |row| row.get::<usize, isize>(0))?;
        let mut total = 0;
        for i in total_vec {
            total += i?;
        }
        config.conn.execute(
            &format! {"UPDATE \"SCORES\" SET \"{team}\"={total} WHERE \"RACEID\"=0;"},
            (),
        )?;
    }
    Ok(())
}

#[tokio::main]
async fn get_race_table(client: &ErgastClient, round: u32, year: u32) -> Result<RaceTable, &str> {
    match client.race_results(Some(year), Some(round)).await {
        Ok(rt) => Ok(rt),
        Err(_) => Err("could not get race results"),
    }
}

fn get_race_results(rt: &RaceTable) -> Result<&Vec<RaceResult>, &'static str> {
    let race = match rt.races.first() {
        Some(race) => race,
        None => return Err("race is not ready yet"),
    };

    let results = match &race.race_results {
        Some(results) => results,
        None => return Err("race is not ready yet"),
    };

    Ok(results)
}

// score functions. will hopefully add more later
fn score_og(results: &[RaceResult], driver: u32) -> Result<isize, Box<dyn Error>> {
    let result = match results.iter().find(|r| r.number == driver) {
        Some(result) => result,
        None => return Err("driver not found in race".into()),
    };
    let mut grid = result.grid;
    if grid == 0 {
        grid = 20;
    }

    Ok((21 - result.position as isize) + (grid as isize - result.position as isize))
}
