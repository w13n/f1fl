use std::process;

use clap::Parser;
use f1fl::Cli;

// TODO:  calculate should support calculating all past non calculations up to the current round, but default to just calculating the current one
//        calculate should support different calculation modes
//        calculate should support recalculating past calculations
//        impliment extentions?
//        view should show all undrafted drivers
fn main() {
    let cli = Cli::parse();

    if let Err(err) = f1fl::run(cli) {
        eprintln!("Error: {err}");
        process::exit(1);
    }
}
