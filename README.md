# F1FL
A command line utility to automatically manage a Formula 1 fantasy sports league. This project is still under developnment and has not released a version 1 yet. As the program is designed for personal use, and has yet to be released, this documentation is a work in progress.

## Commands

### Setup
> `f1fl setup {path} {year} {team_count} {driver_count}`

*creates the season's database and first round draft picks*

ex: `f1fl setup ~/F1FL/2023.db 2023 6 3`
- path: a local path to an empty sqlite database, or where you want the program to create a sqlite database
- year: the four digit year of the Formula 1 season that you want the fantasy season to mirror
- team_count: the number of teams you want to have for the season
- driver_count: the number of drivers per team you want to have for the season
---
### Draft
> `f1fl draft {path} {round}`

*updates the database with each teams draft pick for the given round*

ex: `f1fl draft ~/F1FL/2023.db 12`
- path: the path to the season's database that you want to modify
- round: the Formula 1 race weekend round that you are drafting for
    - ex: first race of the season: 1
    - ex: last race of a 22 race season: 22

**NOTES:**
- Round must be one greater than the highest drafted as it stood round before running
---
### Skip
> `f1fl skip {path} {round}`

*updates the database similar to draft, but without changing the driver lineup for each team*

ex: `f1fl skip ~/F1FL/2023.db 12`
- path: the path to the season's database that you want to modify
- round: the Formula 1 race weekend round that you are drafting for
    - ex: first race of the season: 1
    - ex: last race of a 22 race season: 22

**NOTES:**
- Round must be one greater than the highest drafted as it stood round before running
---
### Score
> `f1fl score {path} {round}`

*scores the round after the race has concluded based on the scoring method setup when the database was setup*

ex: `f1fl score ~/F1FL/2023.db 12`
- path: the path to the season's database that you want to modify
- round: the Formula 1 race weekend round that you are drafting for
    - ex: first race of the season: 1
    - ex: last race of a 22 race season: 22

**NOTES:**
- API for pulling race results is not guaranteed to be accurate (as I have unfortunately discovered)
- API can take a few hours to update after a race has concluded
- API will be depreciated after the 2024 season
---
### View
> `f1fl view {path} {round}`

*displaces the current standings and team lineups for the given round*

ex: `f1fl view ~/F1FL/2023.db 12`
- path: the path to the season's database that you want to modify
- round: the Formula 1 race weekend round that you are drafting for
    - ex: first race of the season: 1
    - ex: last race of a 22 race season: 22

**NOTES:**
- the current standings do not change based on the given round, only team lineups